package model;

import org.tinylog.Logger;

import java.util.Objects;

/**
 * Class to count kcal needed and food amount needed.
 *
 */

public class EtrendKeszito {

    /**
     * A function to count the kcal needed.
     *
     * @param ageS age of participant
     * @param massS  mass of participant in Kg
     * @param gender gender of participant
     * @param dietCelja aim of the diet
     * @return the kcal needed or 0 if an amount of a varible is not correct
     */

    public int kaloriaCalc(String ageS,String massS,String gender,String dietCelja){
        int age = Integer.parseInt(ageS);
        int mass = Integer.parseInt(massS);
         int kcalNeed = 1;
        Logger.info("Kaloriaszukselget kiszamolasa");
        if(Objects.equals(gender,"Ferfi")){
            if(age >= 11 && age <= 18){
                kcalNeed = 12*mass+746;
            }
            else if(age >= 19 && age <= 30){
                kcalNeed = 15*mass+496;
            }
            else if(age >= 31 && age <= 80){
                kcalNeed = 9*mass+829;
            }

        }
        if(Objects.equals(gender,"No")){
            if(age >= 11 && age <= 18){
                kcalNeed = 18*mass+651;
            }
            else if(age >= 19 && age <= 30){
                kcalNeed = 15*mass+679;
            }
            else if(age >= 31 && age <= 80){
                kcalNeed = 12*mass+879;
            }

        }
        if(Objects.equals(dietCelja,"Fogyas")){
            return kcalNeed;
        }
        else if(Objects.equals(dietCelja,"Suly_megtartas")){
            return kcalNeed+500;
        }
        else if(Objects.equals(dietCelja,"Tomegeles")){
            return kcalNeed+1000;
        }


    return 0;
    }

    /**(
     * Counts the number to count the correct amount of food for the participant.
     *
     *
     * @param kalneed is the kcal needed counted by {@link model.EtrendKeszito#kaloriaCalc(String, String, String, String)} function
     * @return the number to count the correct amount of food
     */

    public double aranyszam(int kalneed){
        Logger.info("Aranyszam kiszamolas");
        int ebedkal = kalneed-1290;
        double ebedkald = Double.valueOf(ebedkal);
        return ebedkal/1080.0;
    }

}
