package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import json.JsonWrite;
import model.EtrendKeszito;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;

import static java.lang.Math.round;

public class EtrendController {

    ObservableList<String> genderList = FXCollections.observableArrayList("Ferfi","No");
    ObservableList<String> dietaCeljaList = FXCollections.observableArrayList("Fogyas","Suly_megtartas","Tomegeles");

    @FXML
    private ChoiceBox neme;

    @FXML
    private ChoiceBox dietaCelja;


    @FXML
    private TextField kora;

    @FXML
    private TextField tomege;

    @FXML
    private TextField testmagassaga;

    @FXML
    private Button kesz;

    @FXML
    private Button buttonKi;

    @FXML
    private Label megfeleloEtrend;

    @FXML
    private Label reggeli;

    @FXML
    private Label ebed;

    @FXML
    private Label vacsora;

    private EtrendKeszito etrendKeszito;
    private JsonWrite jsonwrite;

    @FXML
    public void initialize(){
        neme.setItems(genderList);
        dietaCelja.setItems(dietaCeljaList);
        etrendKeszito = new EtrendKeszito();
        jsonwrite = new JsonWrite();
    }


    public void keszF(ActionEvent actionEvent) throws IOException, ParseException {
        int kaloriaszkuseglet = etrendKeszito.kaloriaCalc(kora.getText(),tomege.getText(),neme.getValue().toString(),dietaCelja.getValue().toString());
        double arany = etrendKeszito.aranyszam(kaloriaszkuseglet);
        megfeleloEtrend.setText(String.valueOf(kaloriaszkuseglet));

        reggeli.setText("- banán: 1db (100 kcal)\n- zab: 100g (400kcal)\n- tej: 3dl (150kcal)");

        ebed.setText("- fehér rizs: "+ String.valueOf(round(200.0*arany))+"g ("+ String.valueOf(round(720.0*arany)+"kcal)\n- csirke: "+ String.valueOf(round(300.0*arany))+"g ("+ String.valueOf(round(360.0*arany))+"kcal)"));
        //ebed.setText(String.valueOf(200.0*etrendKeszito.aranyszam(kaloriaszkuseglet)));
        vacsora.setText("- cezar salata: 1adag (650kcal)");


        jsonwrite.jsonbe(kora.getText(),tomege.getText(),neme.getValue().toString(),testmagassaga.getText());
        Logger.info("A kesz gomb lefutott");


    }

    public void adatiokKi(ActionEvent actionEvent) throws FileNotFoundException {

        jsonwrite.jsonki();
    }
}
