package json;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.tinylog.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class to read and write to a json file
 * that saves the datas of the participants.
 *
 */


public class JsonWrite {

    /**
     * Stores the data of the participants in resztvevok.json.
     *
     *
     * @param ageS age of participant
     * @param massS mass of participant
     * @param gender gender of participant
     * @param height height of participant
     * @throws IOException if somethin is wrong with the json file
     * @throws ParseException if something unepected happens while parsing
     */

    public static void jsonbe(String ageS,String massS,String gender,String height) throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        Object obj =jsonParser.parse(new FileReader("resztvevok.json"));
        JSONArray jsonArray = (JSONArray)obj;

        JSONObject resztvevo_adatok = new JSONObject();
        resztvevo_adatok.put("age",ageS);
        resztvevo_adatok.put("mass",massS);
        resztvevo_adatok.put("nem",gender);
        resztvevo_adatok.put("height",height);

        JSONObject resztvevo1 = new JSONObject();
        resztvevo1.put("resztvevo1",resztvevo_adatok);


        jsonArray.add(resztvevo1);

        try (FileWriter file = new FileWriter("resztvevok.json")) {

            file.write(jsonArray.toJSONString());
            file.flush();
            file.close();

        }catch (IOException e) {
            e.printStackTrace();
            Logger.error(new IOException("Ooops"),"IO hiba tortent");
        }
        Logger.info("A resztvevo hozza lett adva a listahoz");
    };


    /**
     * Prints out the data created by jsonbe.
     *
     *
     * Prints out the data created by {@link json.JsonWrite#jsonbe(String, String, String, String)}
     * @throws FileNotFoundException if the resztvevok.json file does not exist
     */
    public void jsonki() throws FileNotFoundException {

        JSONParser jsonparser2 = new JSONParser();
        try (FileReader reader = new FileReader("resztvevok.json")){
            Object obj = jsonparser2.parse(reader);
            JSONArray resztvevo_lista = (JSONArray) obj;
            System.out.println(resztvevo_lista);
            Logger.info("A reszvevok ki lettek irva");



        }catch (FileNotFoundException e) {
            e.printStackTrace();

        }catch(IOException e){
            e.printStackTrace();
            Logger.error(new IOException("Ooops"),"IO hiba tortent");
        }catch (ParseException e){
            e.printStackTrace();
            Logger.error(new ParseException(1),"Parse hiba tortent");
        }
    }


}
