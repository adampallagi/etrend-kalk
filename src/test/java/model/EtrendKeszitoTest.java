package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EtrendKeszitoTest {

    @Test
    void testEtrendKeszito() {
        EtrendKeszito etrendKeszito = new EtrendKeszito();

        int expected = 1546;
        int actual = etrendKeszito.kaloriaCalc(String.valueOf(20),String.valueOf(70),"Ferfi","Fogyas");
        assertEquals(expected,actual);

    }

    @Test
    void testaranyszam(){
        EtrendKeszito etrendKeszito = new EtrendKeszito();
        double expected = 0.23703703703703705;
        double actual = etrendKeszito.aranyszam(1546);
        assertEquals(expected,actual);

    }

}